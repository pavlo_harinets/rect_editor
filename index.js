var rectanglesArray = [];                                   //array for saving rectangles
function Rectangle(x1, y1, x2, y2, color, lineWidth) {            //Rectangle constructor with x,y coords of two angles
    var tmp;                                                //for swapping vars
    if (x1 > x2) {                                          //checking coordinates to calculate width and height of
        tmp = x1;                                           //svg rect selector
        x1 = x2;                                            //i know about shortcode one line swap but i like clear code
        x2 = tmp;
    }
    if (y1 > y2) {                                          //swapping y1 and y2 if first is greater
        tmp = y1;
        y1 = y2;
        y2 = tmp;
    }
    this.x = x1;                                            //setting object x coord
    this.y = y1;                                            //setting object y coord
    this.width = x2 - x1;                                   //calculating width
    this.height = y2 - y1;                                  //calculating height

    this.selector = '<rect x="' + this.x + '" y="' + this.y + '" width="' + this.width + '" height="' + this.height +
        '" stroke="' + color + '" stroke-width="' + lineWidth + '" fill="none"/>';            //forming rect selector for svg
    this.isRectSelected = function (x, y) {
        if ((x >= this.x) && (x <= (this.x + this.width) && (y >= this.y) && (y <= (this.y + this.height)))) {
            return true;                                    //checking dot with coordinates x,y is inside rectangle
        } else {
            return false;
        }
    }
}

var svgBuild = function (rectArray) {                   //builds svg selector from rect objects
    var svgEl = '<svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" ' +
        'width="600" height="600" viewBox="0 0 600 600">';
    rectArray.forEach(function (item, i, arr) {             //all rectangles rects selectors from array concat
        svgEl += item.selector;
    });
    svgEl += '</svg>';
    return svgEl;
};

function SaveAsFile(t,f,m) {                                //saving text to file with filsaver.js lib functions
    try {
        var b = new Blob([t],{type:m});
        saveAs(b, f);
    } catch (e) {
        window.open("data:"+m+"," + encodeURIComponent(t), '_blank','');
    }
}

function saveRectangles(rectArr){
    var svgText = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>'+svgBuild(rectArr);
    SaveAsFile(svgText,"filename.svg","text/plain;charset=utf-8"); //and saving this string to file
}

function decodeRectangles(svgText) {
    var startPos, endPos;
    var rectStrs = [], x , y , width, height, lineWidth, color;
    for (var i = 0; i < svgText.length; i++) { //getting <rect...../> selectors to array of strings
        startPos = svgText.search("<rect");
        if (startPos != -1) {
            svgText = svgText.substr(startPos);
            endPos = svgText.search("/>");
            rectStrs[i] = svgText.substring(0, endPos + 2);
            svgText = svgText.substr(endPos + 2);
        }
    }
    //decoding <rect...../> selectors. can write more elegant decoder but in this case there is no reason to do this
    rectanglesArray = [];
    rectStrs.forEach(function (item, i, arr) {
        startPos = item.search('x="') + 3;                  //a lot of string operations
        endPos = item.search('" y="');
        x = +item.substring(startPos, endPos);
        startPos = endPos + 5;
        endPos = item.search('" width="');
        y = +item.substring(startPos, endPos);
        startPos = endPos + 9;
        endPos = item.search('" height="');
        width = +item.substring(startPos, endPos);
        startPos = endPos + 10;
        endPos = item.search('" stroke="');
        height = +item.substring(startPos, endPos);
        startPos = endPos + 10;
        endPos = item.search('" stroke-width="');
        color = item.substring(startPos, endPos);
        startPos = endPos + 16;
        endPos = item.search('" fill="');
        lineWidth = +item.substring(startPos, endPos);
        rectanglesArray.push(new Rectangle(x,y,x+width,y+height,color,lineWidth));
    });
    $('#drawing').html(svgBuild(rectanglesArray));//fill svg to html code
    return rectanglesArray;                       //returning rectangles array
}

function readFile(event){
    var file = event.target.files[0];                               //accessing filename
    var reader = new FileReader();                                  //creating FileReader
    reader.onload = (function(theFile) {                            //adding event handler
        return function(e) {
            rectanglesArray = decodeRectangles(e.target.result);
        };
    })(file);
    reader.readAsText(file);
}

$(document).ready(function () {

    var initCanvas = function () {                          //function that inits canvas tracking

        var isRectStarted;                                  //is rect drawing started
        var rect = {                                        //rect object with startpoint coords x,y and size width,height
            x: 0,
            y: 0
        };
        $('#saveBtn').click(function(){                     //adding save action to save button
            saveRectangles(rectanglesArray);                //calling save method
        });
        $('#newBtn').click(function(){                      //adding action to new button
            location.reload();                              //reload page from cache, initialize all from 0
        });
        document.getElementById('files').addEventListener('change', readFile, false);
        $('#drawing').mousemove(function (e) {              //function that handles mouse moving tracking
            var coords;                                     //html string for coords
            var x = e.offsetX;                              //X mouse relative coordinate
            var y = e.offsetY;                              //Y mouse relative coordinate
            coords = '<p>X:' + x + 'Y:' + y + '</p>';       //forming p with mouse move coords
            $('#coords').html(coords);                      //writing p into #coords div
        });

        $('#drawing').click(function (e) {                  //function that handles mouse clicking
            var x = e.offsetX;                              //getting mouse click coords x
            var y = e.offsetY;                              //getting mouse click coords y
            var color = $('input[name=color]:checked').val();       //getting color from html
            var lineWidth = $('input[name=width]:checked').val();   //getting line width
            var mode = $('input[name=mode]:checked').val();        //getting work mode (draw,edit)
            if (mode == 1) {                                         //if in a drawer mode draw rect else...
                if (!isRectStarted) {                           //if rect drawing not started
                    rect.x = x;                                 //filling coordinates to rect object
                    rect.y = y;
                    isRectStarted = true;                       //setting flag that we starts drawing of the rect
                } else {
                    isRectStarted = false;                      //setting flag that we finished drawing of the rect
                    rectanglesArray.push(new Rectangle(x, y, rect.x, rect.y, color, lineWidth)); //pushing new object to array
                    //console.log(rectanglesArray);
                    $('#drawing').html(svgBuild(rectanglesArray));//fill svg to html code
                }
            } else {
                rectanglesArray.forEach(function (item, i, array) {//in edit mode checking all rectangles is mouse selected
                    if (item.isRectSelected(x, y)) {                //replacing old rectangle with new if mouse coordinates is inside rect
                        rectanglesArray[i] = new Rectangle(item.x, item.y, item.x + item.width, item.y + item.height, color, lineWidth);
                    }
                });
                $('#drawing').html(svgBuild(rectanglesArray));//fill svg to html code
            }
        });
    };
    initCanvas();                                           //start canvas mouse tracking
});